#This is a modification of Dr. Eric Sayers' Application 2 of Entrez Programming Utilities Help
#Created: April 24, 2009; Last Update: April 23, 2018.
#https://www.ncbi.nlm.nih.gov/books/NBK25498/
#Eric Sayers, PhD
#Modified by: Martey Haw
#Retrieves fasta sequences for a comma-separated list of accession numbers
#Output: Compiled_Fastas.fasta

use LWP::Simple;

#Edit with accession numbers specifically directed to protein coding sequences
$acc_list = 'CCD32610.1';

#Edit with selected database
$database = 'protein';

#Edit with desired filename output
$filename = 'Compiled_Fastas';

#==================================================

@acc_array = split(/,/, $acc_list);

#append [accn] field to each accession
for ($i=0; $i < @acc_array; $i++) {
   $acc_array[$i] .= "[all]";
}

#join the accessions with OR
$query = join('+OR+',@acc_array);

#assemble the esearch URL
$base = 'https://eutils.ncbi.nlm.nih.gov/entrez/eutils/';
$url = $base . "esearch.fcgi?db=" . $database . "&term=$query&usehistory=y";

#post the esearch URL
$output = get($url);

#parse WebEnv and QueryKey
$web = $1 if ($output =~ /<WebEnv>(\S+)<\/WebEnv>/);
$key = $1 if ($output =~ /<QueryKey>(\d+)<\/QueryKey>/);

open(OUT, ">" . $filename . ".fasta") || die "Can't open file!\n";

#assemble the efetch URL
$url = $base . "efetch.fcgi?db=" . $database . "&query_key=$key&WebEnv=$web";
$url .= "&rettype=fasta&retmode=text";

#post the efetch URL
$fasta = get($url);
print OUT "$fasta";

close OUT;

